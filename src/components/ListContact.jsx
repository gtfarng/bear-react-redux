import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class ListContact extends Component {

  deleteContact = (e) => 
  {
    this.props.deleteContactReducer(e);
  }

  render() 
  {
    const { contacts } = this.props;
    console.log("Listcontact",contacts);
    const ListContact = contacts.map(contact => 
      {

      return (

        <h5>
          <li key={contact.id}> {contact.id}. Name : <small>{contact.name}</small> Phone : <small>{contact.phone}</small> E-Mail :  <small>{contact.email}</small> 

          &nbsp; <button class="btn btn-success" onClick={() => this } > GET </button> 
          &nbsp; <button class="btn btn-danger" onClick={() => this.deleteContact(contact.id)} > DELETE </button> &nbsp;

            <Link to={`/editcontact/${contact.id}`}>
              <button class="btn btn-warning" >EDIT </button>
            </Link>
          
          </li>
        </h5>

      )
    })

    return (
      <div className="container">
        <br />
        <h2>Bear Profile</h2>  <br />
        {ListContact}
      </div>
    )
  }
}

const mapStateToProps = state => 
{
  return { contacts: state.contactReducer }
}

const mapDispatchToProps = dispatch => 
{
  return {
    deleteContactReducer: id => {
      dispatch({
        type: "REMOVE_CONTACT",
        id
      })
    }
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ListContact)

