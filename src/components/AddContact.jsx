import React, { Component } from "react";
import { connect } from "react-redux";

class AddContact extends Component {

  state = { name: "" }

  handelChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  addContact = () => {
    this.props.addContactReducer({
      ...this.state,
      id: "" + x++
    })
  }

  render() {
    const { contacts } = this.props;
    console.log("Addcontact", contacts);

    return (
      <div className="container">
        <br /><br />
        <h2>Add</h2><br />

        <input type="text" name="name" placeholder="Name... " onChange={this.handelChange} /> &nbsp; &nbsp; &nbsp;
          <input type="number" name="phone" placeholder="Phone..." onChange={this.handelChange} /> &nbsp; &nbsp; &nbsp;
          <input type="email" name="email" placeholder="E-Mail... " onChange={this.handelChange} /><br /><br />
        <button class="btn btn-primary" onClick={() => this.addContact()} >ADD</button>

      </div>
    )
  }
}

const mapStateToProps = state => {
  return { contacts: state.contactReducer }
}

const mapDispatchToProps = dispatch => {
  return {
    addContactReducer: newcontact => {
      dispatch({
        type: "ADD_CONTACT",
        newcontact
      });
    }
  };
};

var x = 2

export default connect(mapStateToProps, mapDispatchToProps)(AddContact);
