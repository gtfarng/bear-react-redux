import React, { Component } from "react";
import { connect } from "react-redux";

class GetContact extends Component {

  state = { name: "" }

  handelChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  getContact = () => {
    this.props.getContactReducer({
      ...this.state
    })
  }

  render() {
    const { contacts } = this.props;
    console.log("Getcontact", contacts);

    return (
      <div className="container">
        <br /><br />
        <h2>Get</h2><br />

        <input type="text" name="id" placeholder="ID... " onChange={this.handelChange} /> &nbsp; &nbsp; &nbsp;
          <br /><br />
        <button class="btn btn-success"  onClick={() => this.getContact()} >GET</button>

      </div>
    )
  }
}

const mapStateToProps = state => {
  return { contacts: state.contactReducer }
}

const mapDispatchToProps = dispatch => {
  return {
    getContactReducer: contacts => {
      dispatch({
        type: "GET_CONTACT"
        
      });
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GetContact);
