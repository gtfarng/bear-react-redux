import React, { Component } from "react";
import Bear from "./components/Bear";
import { BrowserRouter } from "react-router-dom";
import "./App.css";

export default class App extends Component {
  render() {
    return (
      <div>

        <BrowserRouter>
          <div className="App">
            <Bear />
          </div>
        </BrowserRouter>
      </div>
    )
  }
}


