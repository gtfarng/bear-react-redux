import React from "react";
import ListContact from './ListContact'
import AddContact from './AddContact'
import EditContact from './editcontact'
import GetContact from './GetContact'

const Bear = () => {
  return (

    <div className="App">
      <header className="App-header">
       <br/> <h1>  My-App</h1><br/>
      </header>
   
        <ListContact />
        <AddContact />
        <GetContact />
        <EditContact  />
     
      </div>
  )
}

export default Bear;
