import React, { Component } from "react";
import { connect } from "react-redux";

class EditContact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      phone: "",
      email: ""
    }
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  componentDidMount = () => {
    console.log("Editcontact-State: ",this.state);
    this.setState({
      ...this.props.contacts.filter(e => e._id === this.props._id)[0]  
    })
   
  }

  editContact = () => {
    this.props.editContactReducer({ ...this.state })
  }

  render() {
    console.log("Editcontact-Contact: ",this.props.contacts);
    return (
      <div className="container">
        <br /><br />
        <h2>Edit-Demo</h2><br/>


        <input type="text" name="name" value={this.state.name} onChange={this.handleChange} />&nbsp; &nbsp; &nbsp;
        <input type="text" name="phone" value={this.state.phone} onChange={this.handleChange} />&nbsp; &nbsp; &nbsp;
        <input type="text" name="email" value={this.state.email} onChange={this.handleChange} />&nbsp; &nbsp; &nbsp;<br/><br/>
     
          <button class="btn btn-warning" onClick={this.editContact}>Edit</button>
        
        <br/><br/><br/>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contactReducer
  };
};

const mapDispatchToProps = dispatch => {
  return {
    editContactReducer: editcontact => {
      dispatch({
        type: "EDIT_CONTACT",
        editcontact
      });
    }
  };
};

export default connect(  mapStateToProps,  mapDispatchToProps)(EditContact);
