const initState = [
  {
    id: "0",
    name: "Son goku",
    phone: "15",
    email: "Son_goku@gmail.com"
  },
  {
    id: "1",
    name: "Vegetable",
    phone: "16",
    email: "Vegetable@gmail.com"
  }
];

const contactReducer = (state = initState, action) => 
{
  switch (action.type) 
  {
    case "ADD_CONTACT":
      return state.concat(action.newcontact)

    case "EDIT_CONTACT":
      return state.map(e =>
        e.id === action.editcontact.id ? (e = action.editcontact) : e)

    case "REMOVE_CONTACT":
      return state.filter(e => e.id !== action.id)

      case "GET_CONTACT":
      return state.concat(action.contact)

    default:
      return state;
  }
}

export default contactReducer;
